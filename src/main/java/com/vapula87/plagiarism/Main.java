package com.vapula87.plagiarism; //Default IntelliJ package name
import java.io.File;
import java.util.Scanner;
import com.vapula87.plagiarism.structures.FileGraph;
/**
 * Main driver class for plagiarism detector.
 * Prompts user to enter a folder. When a valid
 * folder is entered, a new FileGraph is created
 * and plagiarism matching begins.
 *
 * @author Michael Hackett
 */
public class Main {
    public static File folder;
    public static int phraseNum, matchNum;
    public static final int MIN_PHRASES = 5, MIN_MATCHES = 200, MIN_DOCUMENTS = 2, INPUT_LENGTH = 3;
    public static void main(String[] args) { new Main(); }
    public Main() {
        Scanner scan = new Scanner(System.in);
        String input = "";
        System.out.println("Plagiarism catcher!");
        //plagiarismCatcher "Suspicious Docs/docs50/" 6 200
        while (!checkInput(input)) input = scan.nextLine();
        new FileGraph();
    }
    /**
     * Verifies that the command entered is valid and that the specified directory exists.
	 *
     * @param input A string entered by the user.
     * @return True if the input is valid. False otherwise.
     */
    private boolean checkInput(String input) {
        boolean good = false;
        String[] splitInput = input.split("\""), splitAgain;
        if (input.equals("")) System.out.println("Format is: plagiarismCatcher \"path/to/file\" [Phrase Length] [Min Occurrences]");
        else if (!splitInput[0].equals("plagiarismCatcher ") || splitInput.length != INPUT_LENGTH) System.out.println("Invalid format.");
        else {
            folder = new File(splitInput[1]);
            if (!folder.isDirectory()) {
                System.out.println("Not a valid directory.");
                return false;
            }
            try {
                splitAgain = splitInput[2].split(" ");
                phraseNum = Integer.parseInt(splitAgain[1]);
                matchNum = Integer.parseInt(splitAgain[2]);
                if (phraseNum < MIN_PHRASES) System.out.println("Minimum number of phrases is "+MIN_PHRASES);
                else if (matchNum < MIN_MATCHES) System.out.println("Minimum number of matches is "+MIN_MATCHES);
                else good = true;
            } catch (Exception x) { System.out.println("Invalid format. Must use integers."); }
        }
        return good;
    }
}