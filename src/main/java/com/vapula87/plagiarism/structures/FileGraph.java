package com.vapula87.plagiarism.structures;
import com.vapula87.plagiarism.Main;
import com.vapula87.plagiarism.interfaces.Entry;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
/**
 * Creates a new undirected graph using text files in a folder as nodes.
 * Each FileNode contains a reference to its parent file as well as a map of pointers to other
 * FileNodes that contain files with matching phrases. Each map entry holds the number of matching
 * phrases between the parent file and the mapped file. Each FileNode combined with its internal
 * adjacency map constitutes a connected component of the undirected graph.
 *
 * @author Michael Hackett
 */
public class FileGraph {
    private ArrayList<FileNode> nodes;
    private AVLTreeMap<Integer,ArrayList<FileNode>> countTree;
    private boolean addIt = false;
    private int tracker;
    private LinkedQueue<FileNode> nodeQueue;
    /**
     * Initializes a new file graph for use in plagiarism detection.
     */
    public FileGraph() {
        countTree = new AVLTreeMap<>();
        nodes = new ArrayList<>();
        nodeQueue = new LinkedQueue<>();
        populateGraph();
        if (nodes.size() < Main.MIN_DOCUMENTS) {
            System.out.println("Nothing to compare.");
            return;
        }
        countPhrases();
        mapPhrases();
        printGroups();
        System.out.println("Finished!");
    }
    /**
     * An inner node class which contains a reference to a file and an adjacency map for use with an undirected graph.
     */
    private class FileNode {
        private File file;
        private AVLTreeMap<FileNode,Integer> commonMap;
        boolean visited = false, enqueued = false;
        public FileNode(File file) {
            this.file = file;
            commonMap = new AVLTreeMap<>(new FileNodeComparator<>());
        }
    }
    /**
     * Populates the graph using text files in a folder.
     */
    private void populateGraph() {
        System.out.println("Populating graph... ");
        File[] files = Main.folder.listFiles();
        for (File x : files) {
            if (!x.toString().contains(".txt")) continue;
            nodes.add(new FileNode(x));
        }
    }
    /**
     * Opens and reads through every file stored in the graph. Unique N word phrases are extracted
     * and stored in an AVL tree. To ensure the stored phrases are unique, they are stripped
     * of all non-alpha characters and converted to an integer using the hashCode() method
     * of the String class. When a file adds (or attempts to add) a hash code to the tree,
     * a reference to its FileNode is placed in an ArrayList. This method essentially amounts
     * to a hashmap with a bucket array for collisions.
     */
    private void countPhrases() {
        System.out.println("Counting phrases...");
        for (FileNode x : nodes) {
            try {
                FileInputStream input = new FileInputStream(x.file);
                byte[] bytes = input.readAllBytes();
                tracker = 0;
                String extractedPhrase = "";
                for (byte z : bytes) {
                    extractedPhrase+=(char) checkByte(z);
                    if (tracker == Main.phraseNum) {
                        int value = extractedPhrase.hashCode();
                        ArrayList<FileNode> tempNode = (ArrayList) countTree.get(value);
                        if (tempNode == null) countTree.put(value, new ArrayList<>(Collections.singletonList(x)));
                        else if (!tempNode.contains(x)) tempNode.add(x);
                        tracker = 0;
                        extractedPhrase = "";
                    }
                }
                input.close();
            }catch (Exception q) { q.printStackTrace(); }
        }
    }
    /**
     * Reads individual bytes. If the character is uppercase, it is converted to
     * lowercase. If the character is non-alpha, 0 is returned. Otherwise an ascii
     * value is returned. Delimited by space and newline characters.
	 *
     * @param x A byte from a file.
     * @return An ascii value if the character is valid. 0 otherwise.
     */
    private int checkByte(int x) {
        int num = 0;
        if ((x == 32 || x == 10) && addIt) {
            addIt = false;
            tracker++;
        }
        if (x > 64 && x < 91) x+=32;
        if (x > 96 && x < 123) num = x;
        if (num > 0) addIt = true;
        return num;
    }
    /**
     * Loops through the AVL tree (hashmap) and checks each node's bucket. FileNodes
     * caught in the bucket are associated with each other using each FileNode's
     * adjacency map. If a FileNode is already in another FileNode's map, the value stored
     * in that particular map entry is increased by 1.
     */
    private void mapPhrases() {
        System.out.println("Mapping phrases...");
        for (Entry x : countTree.entrySet()) {
            ArrayList tempArr = (ArrayList) x.getValue();
            for(int y = 0; y < tempArr.size(); y++) {
                FileNode tempNode = (FileNode) tempArr.get(y);
                for (int z = y + 1; z < tempArr.size(); z++) {
                    int value = 0;
                    FileNode tempNode2 = (FileNode) tempArr.get(z);
                    if (tempNode.commonMap.get(tempNode2) != null) value = tempNode.commonMap.get(tempNode2)+1;
                    tempNode.commonMap.put(tempNode2, value);
                    tempNode2.commonMap.put(tempNode, value);
                }
            }
        }
    }
    /**
     * Uses each FileNode's adjacency map to print out groups of suspiciously similar documents.
     */
    public void printGroups() {
        System.out.println("Suspicious groups...");
        System.out.println("-------------------------");
        for (FileNode x : nodes) {
            if (x.visited) continue;
            printNode(x);
            while (!nodeQueue.isEmpty()) {
                printNode(nodeQueue.dequeue());
                if (nodeQueue.isEmpty()) System.out.println("-------------------------");
            }
        }
    }
    /**
     * Loops through a FileNode's adjacency map. If the value associated with a FileNode
     * within the map is at least as large as the minimum number of matches specified,
     * the value is printed out along with the parent filename and the mapped filename.
     * The mapped FileNode is then added to a queue. After the primary FileNode's adjacency
     * map has been looped through, FileNodes in queue are passed back into this method.
     *
     * @param x A FileNode to be printed.
     */
    private void printNode(FileNode x) {
        x.visited = true;
        String firstFile = x.file.toString(), secondFile;
        for (Entry y : x.commonMap.entrySet()) {
            if ((int) y.getValue() >= Main.matchNum && !((FileNode) y.getKey()).visited ) {
                FileNode secondNode = (FileNode) y.getKey();
                secondFile = secondNode.file.toString();
                int firstLen = firstFile.length();
                int secondLen = secondFile.length();
                firstFile = firstFile.substring(firstFile.lastIndexOf("\\")+1,firstLen);
                secondFile = secondFile.substring(secondFile.lastIndexOf("\\")+1,secondLen);
                System.out.print(y.getValue() +" matches: ");
                System.out.println(firstFile+" <-> "+secondFile);
                if (!secondNode.enqueued) {
                    nodeQueue.enqueue(secondNode);
                    secondNode.enqueued = true;
                }
            }
        }
    }
    /**
     * A dummy comparator used with the adjacency maps of the FileNode class.
     * Converts FileNode pointers to strings and compares them.
     * 
     * @param <E> A FileNode to be compared.
     */
    private class FileNodeComparator<E> implements Comparator<E> {
        @SuppressWarnings({"unchecked"})
        public int compare(E a, E b) throws ClassCastException {
            return ((Comparable<E>) a.toString()).compareTo((E) b.toString());
        }
    }
}